# ConfiGit

Script bash que permite personalizar o Git, criando atalhos (aliases) e, a partir deles, uma ferramenta para controle do fluxo de trabalho.  

## Uso

Conceda permissão de execução `chmod +x configit.sh` e, em seguida, execute `./configit.sh -h` para ver as opções de uso.

## Fluxo de trabalho

O ConfiGit permite a criação de um fluxo de trabalho que mescla o Git Flow e o Gitlab FLow, de acordo com a necessidade do projeto. 

Enquanto fluxo padrão segue o Git Flow, a estrutura é inspirada no Gitlab FLow com a definição de três branches fixas (main, stage e develop), e quatro branches de suporte (feature, release, bugfix e hotfix). No fluxo aqui definido, a branch stage é de uso opcional e pode ser atualizada a partir de uma branch de release.  

Branches e seus significados: 

- main
  - branch de produção
- stage
  - branch de homologação
- develop
  - branch de desenvolvimento
- feature
  - branch de atividade
- release
  - branch de pacote de entrega
- bugfix
  - branch de correcao de erros na branch release
- hotfix
  - branch de correcao de erros na branch main  
  
  
Apresentação visual do fluxo de trabalho:

```
hotfix    ---------------------------------h1----------------  
                                          /  \  
main      -x----------------------------r1----h1-------------
            \                          /        \ 
stage     ---------------------r1----r1----------------------  
              \               /     /             \  
release   ------------------r1----r1-------------------------
                \          /  \  /  \               \ 
bugfix    ---------------------b1----------------------------  
                  \      /            \               \  
develop   ---------x---f1--------------r1--------------h1----   
                    \ /   
feature   ----------f1---------------------------------------  
```

## Gestão do fluxo de trabalho

Após aplicadas as configurações, execute

```bash
git fi
```

para criar e plublicar a branch de desenvolvimento.

### Features

Utilize

```bash
git ffs <nome-ou-numero>
```

para criar e plublicar uma nova branch de atividade.

Estando na branch da atividade, após commit

```bash
git fff
```

para finalizar a atividade. O comando irá integrar a branch da atividade com a branch de desenvolvimento, publicar as alterações e excluir as branchs de atividade.

### Releases

Utilize

```bash
git frs <nome-ou-numero>
```

para a criação e publicação da branch de release.

Na branch da release, após commit

```bash
git frf
```

para finalizar a release. O comando irá integrar a branch da release com as branches principal e de desenvolvimento, criar tag, publicar as alterações, e excluir a branch de release.

### Bugfixes

Utilize

```bash
git fbs <nome-ou-numero>
```

para a criação e publicação da branch de bugfix.

Na branch de bugfix, após commit

```bash
git fbf
```

para finalizar a bugfix. O comando irá integrar a branch de bugfix com a branch de release, publicar as alterações, e excluir a branch de bugfix.

### Hotfixes

Utilize

```bash
git fhs <nome-ou-numero>
```

para a criação e publicação da branch de hotfix.

Na branch de hotfix, após commit

```bash
git fhf
```

para concluir o hotfix. O comando irá integrar a branch da hotfix com as branches principal e de desenvolvimento, criar tag, publicar as alterações e excluir as branchs de hotfix.

### Stage Release

Estando na branch de release, utilize

```bash
git frst 
```

para integrar a branch de release com a branch de stage e publicar as alterações.