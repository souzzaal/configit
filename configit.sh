#!/bin/bash

# Cria configuração persinalizada para o git
# @autor André Souza

OPT=""
LOCATION="--local"
locationLabel='locais'
if [ $LOCATION == '--global' ]; then
	locationLabel='globais'
elif [ $LOCATION == '--system' ]; then
	locationLabel='de sistema'
fi

while getopts 'ghlrs' opt; do
	case "$opt" in
		g)
			LOCATION='--global'
			;;
		h)
			clear
			echo -e "\nConfiGit\n"
			echo -e "Configura atalhos (aliases) para Git \n"
			echo -e "Uso:\n\t $(basename $0) [-g] [-h] [-l] [-r] [-s] \n"
			echo -e "Opções:"
			echo -e "\t -g\tAplica as configurações para o usuario do sistema operacional"
			echo -e "\t -h\tExibe ajuda"
			echo -e "\t -l\tAplica as configurações para um repositório (padrão)"
			echo -e "\t -r\tRemove as configurações, implica -l por padrão"
			echo -e "\t -s\tAplica as configurações para o sistema operacional"
			exit 0
			;;
		r)
			OPT="R"
			;;
		s)
			LOCATION='--system'
			;;
		?)
			echo -e "\e[0;41m Opção inválida! \e[0m Tente $(basename $0) -h para obter ajuda."
			exit 1
			;;
	esac
done

# Removendo configuracoes previas
## Atalhos
configFilePath=$(git config --list $LOCATION --show-origin | awk '{print $1}' | uniq | sed \s/'file:'//)
git config $LOCATION --get-regexp alias | while read line ; do words=($line) && git config $LOCATION --unset ${words[0]}; done
sed -i '/^\[alias\]$/d' $configFilePath		# Remove idetificador da secao 'alias'

## Configuracoes gerais
if [ "$OPT" == "R" ]; then
	# Limpa configuracoes de cor 
	git config $LOCATION --unset color.ui
	
	# Limpa informacoes do usuario
	git config $LOCATION --unset user.name
	git config $LOCATION --unset user.email

	# Remove secoes de identificaca do arquivo de configuracao (.gitconfig)
	sed -i '/^\[color\]$/d' $configFilePath
	sed -i '/^\[user\]$/d' $configFilePath

	if [ $? -eq 0 ]; then
		echo -e "\e[0;92mSucesso! \e[0mConfigurações $locationLabel removidas."
	fi
	exit 1;
fi
# Aplicando as novas configurações
## Cor
git config $LOCATION color.ui true

## Informacoes de usuario
if [[ -z $(git config $LOCATION user.name) ]]; then
	echo -e "Informe seu nome: "
	read userName
	git config $LOCATION user.name $userName
fi

if [[ -z $(git config $LOCATION user.email) ]]; then
	echo -e "Informe seu e-mail: "
	read userEmail
	git config $LOCATION user.email $userEmail
fi

## Aliases
### Add
git config $LOCATION alias.a "!git add \$@ && git st"
git config $LOCATION alias.ua "!git rs \$@ && git st"	# desfaz git add
### Branch
git config $LOCATION alias.b branch
git config $LOCATION alias.ba "branch -a"
git config $LOCATION alias.bd "!f(){ git branch -d \$1; };f"
git config $LOCATION alias.bda "!f(){ git branch -d \$1 && git push origin --delete \$1; };f"
git config $LOCATION alias.bdf "!git branch -D"
git config $LOCATION alias.bdr "!f(){ git push origin --delete \$1; };f"
git config $LOCATION alias.bn "!f(){ \
	R=\$(git branch --show-current 2>&-) && \
	if [ \$? -eq 0 ]; then \
		echo \$R; \
	else \
		echo \$(git b --list | grep -e '^\*\s' | sed \s/'* '//); fi;\
};f"
### Checkout
git config $LOCATION alias.bb "checkout -"
git config $LOCATION alias.co checkout
git config $LOCATION alias.cob "checkout -b" 
git config $LOCATION alias.copl "!f(){ git co \$1 && git pull; };f"
### Commit
git config $LOCATION alias.c commit
git config $LOCATION alias.cm "commit -m"
git config $LOCATION alias.ca "commit --amend"
git config $LOCATION alias.cp "!f(){ git c && git push; };f"
git config $LOCATION alias.ucs "!git rss1 && git st"							# desfaz commit - arquivos sao mantidos no stage index
git config $LOCATION alias.ucw "!git rss1 && git st && git ua . && git st"		# desfaz commit	e add - arquivos voltam para o working directory
### Config
git config $LOCATION alias.cel "config -e --local"
git config $LOCATION alias.ceg "config -e --global"
git config $LOCATION alias.cl "config -l"
git config $LOCATION alias.cll "config -l --local"
git config $LOCATION alias.clg "config -l --global"
git config $LOCATION alias.cso "config -l --show-origin"
### Grep
git config $LOCATION alias.g "grep --heading -n -C 5 -p"			# filtra pelo conteudo atual dos arquivos no indice de staging (staging index) ou historico de commits (commit history)
### Log
git config $LOCATION alias.l log
git config $LOCATION alias.lo "log --oneline"
#### Log - Filtros
git config $LOCATION alias.la "!f(){ git l --author=\$1; };f"		# por author do commit
git config $LOCATION alias.lg "!f(){ git l --grep=\$1; };f"			# pela mensagem do commit
git config $LOCATION alias.lgi "!f(){ git l -i --grep=\$1; };f"		# pela mensagem do commit - case insensitive
git config $LOCATION alias.ls "!f(){ git l -S\"\$1\" -p; };f"		# pela conteudo do commit, quando o termo pesquisado foi adicionado, modificado ou removido
### Merge
git config $LOCATION alias.mg merge
git config $LOCATION alias.mga "merge --abort"
git config $LOCATION alias.mgc "merge --continue"
git config $LOCATION alias.mgn "merge --no-ff"
## Pull
git config $LOCATION alias.pl pull
### Push
git config $LOCATION alias.ps push
git config $LOCATION alias.pu "!f(){ git push -u origin \$(git bn); };f"
git config $LOCATION alias.psmr "!f(){ \
	if [ -z \$1 ]; then \
		git ee \"Informe a branch alvo\"; \
		return 1; \
	fi; \
	if [ -z \"\$2\" ]; then \
		title=\"\$(git log --format=%s -1)\"; \
	else \
		title=\"\$2\"; \
	fi; \
	git push -o merge_request.create \
		-o merge_request.target=\$1 \
		-o merge_request.title=\"\$title\" \
		-o merge_request.assign=\"\$(git config user.email)\"; \
};f"
### Remote
git config $LOCATION alias.rmv "remote -v"
### Reset
git config $LOCATION alias.rss "reset --soft" 			# do historico de commits (commit history) para indice de staging (stage index)
git config $LOCATION alias.rss1 "reset --soft HEAD~1" 	# ultimo commit
### Restore
git config $LOCATION alias.rs "restore --staged" 		# do indice de staging (stage index) para diretorio de trabalho (working dirctory)
### Status
git config $LOCATION alias.st status
### Echo
git config $LOCATION alias.es "!f(){ echo \"\n\e[1;42m \$1 \e[49m \e[0;92m \$2 \e[0m \n\"; };f"
git config $LOCATION alias.ee "!f(){ echo \"\n\e[0;41m \$1 \e[49m \e[1;31m \$2 \e[0m \n\"; };f"
git config $LOCATION alias.ei "!f(){ echo \"\n\e[0;44m \$1 \e[49m \e[1;34m \$2 \e[0m \n\"; };f"
### Micro flow
#### New feature
git config $LOCATION alias.nf "!git cob \$1 && git pu"
#### Feature finish
git config $LOCATION alias.ff "!f(){ \
	if [ -z \"\$1\" ]; then \
		git ee 'Informe a branch de retorno'; \
		return 1; \
	fi; \
	B=\$(git bn); \
	git copl \$1; \
	git bd \$B; \
};f"
### Git flow (manual)
#### Flow init
git config $LOCATION alias.fi "!f(){ \
	git es \"Iniciando Fluxo\" && \
	git ei 'i' \"Acessando e atualizando 'main'\" && \
	git copl main && \
	git ei 'i' \"Criando e pulicando 'stage'\" && \
 	git cob stage && \
	git pu && \
	git bb && \
	git ei 'i' \"Criando e pulicando 'develop'\" && \
 	git cob develop && \
 	git pu \
	git es 'ok' 'Sucesso'; \
};f"
#### Flow feature start
git config $LOCATION alias.ffs "!f(){ \
	F=\"feature/\$1\" && \
	git es '' \"Iniciando \$F\" && \
	git ei 'i' \"Acessando e atualizando 'develop'\" && \
	git copl develop && \
	git ei 'i' \"Criando e publicando branch para \$F\" && \
	git cob \$F && \
	git pu && \
	git es 'ok' 'Sucesso'; \
};f"
#### Flow feature finish
git config $LOCATION alias.fff "!f(){ \
	B=\$(git bn) && \
	git es '' \"Finalizando \$B\" && \
	git ei 'i' \"Publicando '\$B'\" && \
	git ps -q && \
	git ei 'i' \"Acessando 'develop'\" && \
	git copl develop && \
	git ei 'i' \"Mesclando '\$B' com 'develop'\" && \
	git mg \$B && \
	git ei 'i' \"Publicando 'develop'\" && \
	git ps && \
	git ei 'i' \"Removendo branches\" && \
	git bda \$B && \
	git es 'ok' 'Sucesso'; \
};f"
#### Flow release start
git config $LOCATION alias.frs "!f(){ \
	R=\"release/\$1\" && \
	git es '' \"Criando \$R\" && \
	git ei 'i' \"Acessando e atualizando 'develop'\" && \
	git copl develop && \
	git ei 'i' \"Criando e publicando \$R\" && \
	git cob \$R && \
	git pu && \
	git es 'ok' 'Sucesso'; \
};f"
#### Flow release finish
git config $LOCATION alias.frf "!f(){ \
	B=\$(git bn) && \
	git es '' \"Finalizando \$B\" && \
	git ei 'i' \"Publicando '\$B'\" && \
	git ps -q && \
	git ei 'i' \"Acessando e atualizando 'main'\" && \
	git copl main && \
	git ei 'i' \"Mesclando \$B com 'main'\" && \
	git mg \$B && \
	git ei 'i' \"Publicando 'main'\" && \
	git ps && \
	git ei 'i' \"Criando e publicando tag'\" && \
	echo 'Informe o nome da tag' && read tagName && \
	git tag -a \$tagName && git push origin \$tagName && \
	git ei 'i' \"Acessando e atualizando 'develop'\" && \
	git copl develop && \
	git ei 'i' \"Mesclando \$B com 'develop'\" && \
	git mg \$B && \
	git ei 'i' \"Publicando 'develop'\" && \
	git ps && \
	git ei 'i' \"Removendo branches\" && \
	git bda \$B && \
	git es 'ok' 'Sucesso'; \
};f"
#### Flow release stage
git config $LOCATION alias.frst "!f(){ \
	B=\$(git bn) && \
	git ei 'i' \"Acessando e atualizando 'stage'\" && \
	git copl stage && \
	git ei 'i' \"Mesclando \$B com 'stage'\" && \
	git mg \$B && \
	git ei 'i' \"Publicando 'stage'\" && \
	git ps && \
	git bb && \
	git es 'ok' 'Sucesso'; \
};f"
#### Flow bugfix start
git config $LOCATION alias.fbs "!f(){ \
	B=\$(git bn) && \
	R=\"\${B}_bugfix/\$1\" && \
	git es '' \"Criando \$R\" && \
	git ei 'i' \"Criando e publicando \$R\" && \
	git cob \"\$R\" && \
	git pu && \
	git es 'ok' 'Sucesso'; \
};f"
#### Flow bugfix finish
git config $LOCATION alias.fbf "!f(){ \
	B=\$(git bn) && \
	R=\$(echo \$B | cut -d'_' -f 1) && \
	git es '' \"Finalizando \$B\" && \
	git ps && \
	git ei 'i' \"Acessando e atualizando \$R\" && \
	git copl \$R && \
	git ei 'i' \"Mesclando \$B com \$R\" && \
	git mg \$B && \
	git ei 'i' \"Publicando \$R\" && \
	git ps && \
	git ei 'i' \"Removendo branches\" && \
	git bda \$B && \
	git es 'ok' 'Sucesso'; \
};f"
#### Flow hotfix start
git config $LOCATION alias.fhs "!f(){ \
	R=\"hotfix/\$1\" && \
	git es '' \"Criando \$R\" && \
	git ei 'i' \"Acessando e atualizando 'main'\" && \
	git copl main && \
	git ei 'i' \"Criando e publicando \$R\" && \
	git cob \$R && \
	git pu && \
	git es 'ok' 'Sucesso'; \
};f"
#### Flow hotfix finish
git config $LOCATION alias.fhf "!f(){ \
	B=\$(git bn) && \
	git es '' \"Finalizando \$B\" && \
	git ei 'i' \"Acessando e atualizando 'main'\" && \
	git copl main && \
	git ei 'i' \"Mesclando \$B com 'main'\" && \
	git mg \$B && \
	git ei 'i' \"Publicando 'main'\" && \
	git ps && \
	git ei 'i' \"Criando e publicando tag'\" && \
	echo 'Informe o nome da tag' && read tagName && \
	git tag -a \$tagName && git push origin \$tagName && \
	git ei 'i' \"Acessando e atualizando 'develop'\" && \
	git copl develop && \
	git ei 'i' \"Mesclando \$B com 'develop'\" && \
	git mg \$B && \
	git ei 'i' \"Publicando 'develop'\" && \
	git ps && \
	git ei 'i' \"Removendo branches\" && \
	git bda \$B && \
	git es 'ok' 'Sucesso'; \
};f"

echo -e "\e[0;92mSucesso! \e[0mConfigurações $locationLabel atualizadas."